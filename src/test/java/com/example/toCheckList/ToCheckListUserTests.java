package com.example.toCheckList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDateTime;
import java.util.LinkedList;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;

import com.example.toCheckList.media.bdd.Media;
import com.example.toCheckList.media.bdd.MediaType;
import com.example.toCheckList.media.resource.MediaRepository;
import com.example.toCheckList.security.JWTUtils;
import com.example.toCheckList.toCheck.bdd.ToCheck;
import com.example.toCheckList.toCheck.bdd.ToCheckState;
import com.example.toCheckList.toCheck.resource.ToCheckRepository;
import com.example.toCheckList.user.bdd.User;
import com.example.toCheckList.user.resource.UserRepository;

@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase(replace = Replace.ANY)
class ToCheckListUserTests {

	@LocalServerPort
	private int port;
	
	@Autowired
	private TestRestTemplate restTemplate;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ToCheckRepository toCheckRepository;
	
	@Autowired
	private MediaRepository mediaRepository;
	
	@Test
	public void testHelloWorld() throws Exception {
		String response = restTemplate.getForObject("http://localhost:" + port + "/rest/test/hello", String.class);
		assertTrue(response.contains("Hello World"));
	}
	
	@Test
	public void testCreateAccount() throws Exception {
		// TEST 1 : NOMINAL
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", "application/json");
		HttpEntity<String> httpEntity = new HttpEntity<>("{\"username\": \"hugandhug\", \"passwordHash\": \"hashhash\"}", headers);
		ResponseEntity<String> creationResponse = restTemplate.postForEntity("http://localhost:" + port + "/rest/user", httpEntity, String.class);
		assertEquals(200, creationResponse.getStatusCodeValue());
		
		// TEST 2 : USERNAME ALLREADY USED
		httpEntity = new HttpEntity<>("{\"username\": \"hugandhug\", \"passwordHash\": \"hashhashDiff\"}", headers);
		ResponseEntity<String> sameNameResponse = restTemplate.postForEntity("http://localhost:" + port + "/rest/user", httpEntity, String.class);
		assertEquals(406, sameNameResponse.getStatusCodeValue());
	}
	
	@Test
	public void testLogin() throws Exception {
		// Preparation
		User user = new User(null, "hugandhug", "passwordHash", new LinkedList<>(), new LinkedList<>());
		user = userRepository.save(user);
		
		// TEST 1 : NOMINAL
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", "application/json");
		HttpEntity<String> httpEntity = new HttpEntity<>("{\"username\": \"hugandhug\", \"passwordHash\": \"passwordHash\"}", headers);
		ResponseEntity<String> loginResponse = restTemplate.postForEntity("http://localhost:" + port + "/rest/user/login", httpEntity, String.class);
		assertEquals(200, loginResponse.getStatusCodeValue());
		String token = loginResponse.getBody();
		assertEquals(user.getId(), JWTUtils.getInstance().getUserId(token));
		
		// TEST 2 : BAD PASSWORD
		httpEntity = new HttpEntity<>("{\"username\": \"hugandhug\", \"passwordHash\": \"password\"}", headers);
		loginResponse = restTemplate.postForEntity("http://localhost:" + port + "/rest/user/login", httpEntity, String.class);
		assertEquals(403, loginResponse.getStatusCodeValue());
		
		// TEST 3 : BAD USERNAME
		httpEntity = new HttpEntity<>("{\"username\": \"stolas\", \"passwordHash\": \"password\"}", headers);
		loginResponse = restTemplate.postForEntity("http://localhost:" + port + "/rest/user/login", httpEntity, String.class);
		assertEquals(403, loginResponse.getStatusCodeValue());
	}
	
	@Test
	public void testDeleteAcccount() throws Exception {
		User user = new User(null, "hugandhug", "passwordHash", null, null);
		user = userRepository.save(user);
		String token = JWTUtils.getInstance().generateToken(user.getId());
		
		Media media = new Media(null, "SpindelHorse Toons", "Helluva Boss", MediaType.SERIES, "");
		media = mediaRepository.save(media);
		ToCheck toCheck = new ToCheck(null, media, "", true, LocalDateTime.now(), ToCheckState.CHECKED, user);
		toCheck = toCheckRepository.save(toCheck);
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", "Bearer " + token);
		HttpEntity<String> httpEntity = new HttpEntity<>(headers);
		ResponseEntity<String> response = restTemplate.exchange("http://localhost:" + port + "/rest/user", HttpMethod.DELETE, httpEntity, String.class);
		assertEquals(204, response.getStatusCodeValue());
		assertTrue(userRepository.findById(user.getId()).isEmpty());
		assertTrue(toCheckRepository.findById(toCheck.getId()).isEmpty());
		assertTrue(mediaRepository.findById(media.getId()).isEmpty());
	}
	
	@Test
	public void testPatchInformations() throws Exception {
		User user = new User(null, "hugandhug", "passwordHash", null, null);
		user = userRepository.save(user);
		String token = JWTUtils.getInstance().generateToken(user.getId());
		
		// Test 1 : username change
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", "Bearer " + token);
		headers.set("Content-Type", "application/json");
		HttpEntity<String> httpEntity = new HttpEntity<String>("{"
				+ "\"username\":\"Stolas\""
				+ "}", headers);
		ResponseEntity<String> response = restTemplate.exchange("http://localhost:" + port + "/rest/user", HttpMethod.PATCH, httpEntity, String.class);
		assertEquals(204, response.getStatusCodeValue());
		user = userRepository.findById(user.getId()).get();
		assertEquals("Stolas", user.getUsername());
		assertEquals("passwordHash", user.getPasswordHash());

		// Test 2 : password change
		httpEntity = new HttpEntity<String>("{"
				+ "\"passwordHash\":\"SuperMDP\""
				+ "}", headers);
		response = restTemplate.exchange("http://localhost:" + port + "/rest/user", HttpMethod.PATCH, httpEntity, String.class);
		assertEquals(204, response.getStatusCodeValue());
		user = userRepository.findById(user.getId()).get();
		assertEquals("Stolas", user.getUsername());
		assertEquals("SuperMDP", user.getPasswordHash());
		
		// Test 3 : both change
		httpEntity = new HttpEntity<String>("{"
				+ "\"passwordHash\":\"MotDePasse\","
				+ "\"username\":\"Blitz\""
				+ "}", headers);
		response = restTemplate.exchange("http://localhost:" + port + "/rest/user", HttpMethod.PATCH, httpEntity, String.class);
		assertEquals(204, response.getStatusCodeValue());
		user = userRepository.findById(user.getId()).get();
		assertEquals("Blitz", user.getUsername());
		assertEquals("MotDePasse", user.getPasswordHash());
	}

}

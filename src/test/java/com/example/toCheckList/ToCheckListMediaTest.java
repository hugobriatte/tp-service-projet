package com.example.toCheckList;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;

import com.example.toCheckList.media.bdd.Media;
import com.example.toCheckList.media.bdd.MediaType;
import com.example.toCheckList.media.resource.MediaRepository;
import com.example.toCheckList.security.JWTUtils;
import com.example.toCheckList.toCheck.bdd.ToCheck;
import com.example.toCheckList.toCheck.bdd.ToCheckState;
import com.example.toCheckList.toCheck.resource.ToCheckRepository;
import com.example.toCheckList.user.bdd.PublicViewer;
import com.example.toCheckList.user.bdd.User;
import com.example.toCheckList.user.resource.UserRepository;

@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase(replace = Replace.ANY)
class ToCheckListMediaTest {

	@LocalServerPort
	private int port;
	
	@Autowired
	private TestRestTemplate restTemplate;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ToCheckRepository toCheckRepository;
	
	@Autowired
	private MediaRepository mediaRepository;
	
	@Test
	void testGetMedia() {
		User user = new User(null, "hugandhug", "passwordHash", new LinkedList<>(), new LinkedList<>());
		user = userRepository.save(user);
		String token = JWTUtils.getInstance().generateToken(user.getId());
		Media media = new Media(null, "SpindelHorse Toons", "Helluva Boss", MediaType.SERIES, "");
		media = mediaRepository.save(media);
		ToCheck toCheck1 = new ToCheck(null, media, "", true, LocalDateTime.now(), ToCheckState.CHECKED, user);
		toCheck1 = toCheckRepository.save(toCheck1);
		
		// TEST 1 : Nominal
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", "Bearer " + token);
		HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
		ResponseEntity<Media> mediaResponse = restTemplate.exchange("http://localhost:" + port + "/rest/media/" + media.getId(), HttpMethod.GET, httpEntity, Media.class);
		assertEquals(200, mediaResponse.getStatusCodeValue());
		Media mediaGained = mediaResponse.getBody();
		assertEquals(media, mediaGained);
		
		// TEST 2 : Unauthorized
		Media media2 = new Media(null, "SpindelHorse Toons", "Helluva Boss", MediaType.SERIES, "");
		media2 = mediaRepository.save(media2);
		User user2 = new User(null, "hugandhuggg", "passwordHash", new LinkedList<>(), new LinkedList<>());
		user2 = userRepository.save(user2);
		ToCheck toCheck2 = new ToCheck(null, media2, "", true, LocalDateTime.now(), ToCheckState.CHECKED, user2);
		toCheck2 = toCheckRepository.save(toCheck2);
		ResponseEntity<String> response = restTemplate.exchange("http://localhost:" + port + "/rest/media/" + media2.getId(), HttpMethod.GET, httpEntity, String.class);
		assertEquals(403, response.getStatusCodeValue());
		
		// TEST 3 : Not found
		response = restTemplate.exchange("http://localhost:" + port + "/rest/media/" + (media2.getId() + 10), HttpMethod.GET, httpEntity, String.class);
		assertEquals(404, response.getStatusCodeValue());
	}
	
	@Test
	void testGetAllUserOfMedia() {
		User user = new User(null, "hugandhug", "passwordHash", new LinkedList<>(), new LinkedList<>());
		user = userRepository.save(user);
		User user2 = new User(null, "Ryunosuke", "passwordHash", new LinkedList<>(), new LinkedList<>());
		user2 = userRepository.save(user2);
		User user3 = new User(null, "Susato", "passwordHash", new LinkedList<>(), new LinkedList<>());
		user3 = userRepository.save(user3);
		Media media = new Media(null, "SpindelHorse Toons", "Helluva Boss", MediaType.SERIES, "");
		media = mediaRepository.save(media);
		ToCheck toCheck1 = new ToCheck(null, media, "", false, LocalDateTime.now(), ToCheckState.CHECKED, user);
		toCheck1 = toCheckRepository.save(toCheck1);
		ToCheck toCheck2 = new ToCheck(null, media, "", false, LocalDateTime.now(), ToCheckState.CHECKED, user2);
		toCheck2 = toCheckRepository.save(toCheck2);
		ToCheck toCheck3 = new ToCheck(null, media, "", true, LocalDateTime.now(), ToCheckState.CHECKED, user3);
		toCheck3 = toCheckRepository.save(toCheck3);
		
		/// TEST 1 : Nominal
		String token = JWTUtils.getInstance().generateToken(user.getId());
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", "Bearer " + token);
		HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
		ResponseEntity<PublicViewer[]> userResponse = restTemplate.exchange("http://localhost:" + port + "/rest/media/" + media.getId() + "/users", HttpMethod.GET, httpEntity, PublicViewer[].class);
		assertEquals(200, userResponse.getStatusCodeValue());
		List<PublicViewer> array = Arrays.asList(userResponse.getBody());
		assertTrue(array.contains(new PublicViewer(user.getId(), user.getUsername(), toCheck1.getToCheckState())));
		assertTrue(array.contains(new PublicViewer(user2.getId(), user2.getUsername(), toCheck2.getToCheckState())));
		assertFalse(array.contains(new PublicViewer(user3.getId(), user3.getUsername(), toCheck3.getToCheckState())));
	}

}

package com.example.toCheckList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.LinkedList;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;

import com.example.toCheckList.media.bdd.Media;
import com.example.toCheckList.media.bdd.MediaType;
import com.example.toCheckList.media.resource.MediaRepository;
import com.example.toCheckList.security.JWTUtils;
import com.example.toCheckList.toCheck.bdd.ToCheck;
import com.example.toCheckList.toCheck.bdd.ToCheckState;
import com.example.toCheckList.toCheck.resource.ToCheckRepository;
import com.example.toCheckList.user.bdd.User;
import com.example.toCheckList.user.resource.UserRepository;

@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase(replace = Replace.ANY)
class ToCheckListToCheckTest {

	@LocalServerPort
	private int port;
	
	@Autowired
	private TestRestTemplate restTemplate;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ToCheckRepository toCheckRepository;
	
	@Autowired
	private MediaRepository mediaRepository;
	
	@Test
	public void testPostToCheck() throws Exception {
		User user = new User(null, "hugandhug", "passwordHash", new LinkedList<>(), new LinkedList<>());
		user = userRepository.save(user);
		String token = JWTUtils.getInstance().generateToken(user.getId());
		
		// TEST 1 : Nominal
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", "Bearer " + token);
		headers.set("Content-Type", "application/json");
		HttpEntity<String> httpEntity = new HttpEntity<>("{"
				+ "\"commentaries\":\"\","
				+ "\"isPrivate\":false,"
				+ "\"toCheckState\":\"CHECKED\","
				+ "\"media\":{"
				+ "		\"name\":\"Helluva Boss\","
				+ "		\"author\":\"SpindleHorse Toons\","
				+ "		\"type\":\"SERIES\","
				+ "		\"comment\":\"\""
				+ "}}", headers);
		ResponseEntity<ToCheck> toCheckResponse = restTemplate.postForEntity("http://localhost:" + port + "/rest/toCheck", httpEntity, ToCheck.class);
		assertEquals(200, toCheckResponse.getStatusCodeValue());
		ToCheck toCheck = toCheckResponse.getBody();
		assertEquals(false, toCheck.getIsPrivate());
		assertEquals(ToCheckState.CHECKED, toCheck.getToCheckState());
		assertEquals("", toCheck.getCommentaries());
		
		// TEST 2 : Nominal with reuse of media
		User userTwo = new User(null, "JeanPaul", "passwordHash", new LinkedList<>(), new LinkedList<>());
		userTwo = userRepository.save(userTwo);
		String tokenTwo = JWTUtils.getInstance().generateToken(userTwo.getId());
		headers = new HttpHeaders();
		headers.set("Authorization", "Bearer " + tokenTwo);
		headers.set("Content-Type", "application/json");
		httpEntity = new HttpEntity<>("{"
				+ "\"commentaries\":\"\","
				+ "\"isPrivate\":true,"
				+ "\"toCheckState\":\"NOT_CHECKED\","
				+ "\"media\":{"
				+ "			\"id\":" + toCheck.getMedia().getId()
				+ "}}", headers);
		toCheckResponse = restTemplate.postForEntity("http://localhost:" + port + "/rest/toCheck", httpEntity, ToCheck.class);
		assertEquals(200, toCheckResponse.getStatusCodeValue());
		ToCheck toCheckTwo = toCheckResponse.getBody();
		assertEquals(true, toCheckTwo.getIsPrivate());
		assertEquals(ToCheckState.NOT_CHECKED, toCheckTwo.getToCheckState());
		assertEquals("", toCheckTwo.getCommentaries());
		assertEquals(toCheck.getMedia().getId(), toCheckTwo.getMedia().getId());
		
		// TEST 3 : Non-existent media id
		httpEntity = new HttpEntity<>("{"
				+ "\"commentaries\":\"\","
				+ "\"isPrivate\":true,"
				+ "\"creationDate\":\"2022-03-25T17:46:03\","
				+ "\"toCheckState\":\"NOT_CHECKED\","
				+ "\"media\":{"
				+ "			\"id\":" + (toCheck.getMedia().getId() + 1)
				+ "}}", headers);
		ResponseEntity<String> response = restTemplate.postForEntity("http://localhost:" + port + "/rest/toCheck", httpEntity, String.class);
		assertEquals(404, response.getStatusCodeValue());
		
		// TEST 4 : Non-existent user 
		headers.set("Authorization", "Bearer " + token + "abcd");
		httpEntity = new HttpEntity<>("{"
				+ "\"commentaries\":\"\","
				+ "\"isPrivate\":true,"
				+ "\"creationDate\":\"2022-03-25T17:46:03\","
				+ "\"toCheckState\":\"NOT_CHECKED\","
				+ "\"media\":{"
				+ "			\"id\":" + toCheck.getMedia().getId()
				+ "}}", headers);
		response = restTemplate.postForEntity("http://localhost:" + port + "/rest/toCheck", httpEntity, String.class);
		assertEquals(401, response.getStatusCodeValue());
	}
	
	@Test
	public void testIsEditable() throws Exception {
		// STEP 1 : Creation of two toCheck, one private, one public
		User user1 = new User(null, "hugandhug", "passwordHash", new LinkedList<>(), new LinkedList<>());
		user1 = userRepository.save(user1);
		User user2 = new User(null, "hugandhuggg", "passwordHash", new LinkedList<>(), new LinkedList<>());
		user2 = userRepository.save(user2);
		String token1 = JWTUtils.getInstance().generateToken(user1.getId());
		String token2 = JWTUtils.getInstance().generateToken(user2.getId());
		
		Media media = new Media(null, "SpindelHorse Toons", "Helluva Boss", MediaType.SERIES, "");
		media = mediaRepository.save(media);
		ToCheck toCheck1 = new ToCheck(null, media, "", true, LocalDateTime.now(), ToCheckState.CHECKED, user1);
		toCheck1 = toCheckRepository.save(toCheck1);
		ToCheck toCheck2 = new ToCheck(null, media, "", false, LocalDateTime.now(), ToCheckState.CHECKED, user2);
		toCheck2 = toCheckRepository.save(toCheck2);

		// Verification of step 1
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", "Bearer " + token1);
		ResponseEntity<String> response = restTemplate.exchange("http://localhost:" + port + "/rest/toCheck/" + toCheck1.getId() + "/isMediaEditable", HttpMethod.GET, new HttpEntity<>(headers), String.class);
		assertEquals(200, response.getStatusCodeValue());
		assertEquals("false", response.getBody());
		
		headers.set("Authorization", "Bearer " + token2);
		response = restTemplate.exchange("http://localhost:" + port + "/rest/toCheck/" + toCheck2.getId() + "/isMediaEditable", HttpMethod.GET, new HttpEntity<>(headers), String.class);
		assertEquals(200, response.getStatusCodeValue());
		assertEquals("true", response.getBody());
		
		// Step 2 : Creation of a earlier editable
		User user3 = new User(null, "hugandhuggggg", "passwordHash", new LinkedList<>(), new LinkedList<>());
		user3 = userRepository.save(user3);
		String token3 = JWTUtils.getInstance().generateToken(user3.getId());
		ToCheck toCheck3 = new ToCheck(null, media, "", false, LocalDateTime.now().minus(1, ChronoUnit.DAYS), ToCheckState.CHECKED, user3);
		toCheck3 = toCheckRepository.save(toCheck3);
		
		// Verification of step 2
		headers.set("Authorization", "Bearer " + token3);
		response = restTemplate.exchange("http://localhost:" + port + "/rest/toCheck/" + toCheck3.getId() + "/isMediaEditable", HttpMethod.GET, new HttpEntity<>(headers), String.class);
		assertEquals(200, response.getStatusCodeValue());
		assertEquals("true", response.getBody());
		
		headers.set("Authorization", "Bearer " + token2);
		response = restTemplate.exchange("http://localhost:" + port + "/rest/toCheck/" + toCheck2.getId() + "/isMediaEditable", HttpMethod.GET, new HttpEntity<>(headers), String.class);
		assertEquals(200, response.getStatusCodeValue());
		assertEquals("false", response.getBody());
	}
	
	@Test
	public void testPutToCheck() {
		User user = new User(null, "hugandhug", "passwordHash", new LinkedList<>(), new LinkedList<>());
		user = userRepository.save(user);
		String token = JWTUtils.getInstance().generateToken(user.getId());
		Media media = new Media(null, "SpindelHorse Toons", "Helluva Boss", MediaType.SERIES, "");
		media = mediaRepository.save(media);
		ToCheck toCheck = new ToCheck(null, media, "", true, LocalDateTime.now(), ToCheckState.CHECKED, user);
		toCheck = toCheckRepository.save(toCheck);
	
		// TEST 1 : Nominal
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", "Bearer " + token);
		headers.set("Content-Type", "application/json");
		HttpEntity<String> httpEntity = new HttpEntity<>("{"
				+ "\"commentaries\":\"Bonjour\","
				+ "\"isPrivate\":true,"
				+ "\"toCheckState\":\"NOT_CHECKED\""
				+ "}", headers);
		ResponseEntity<ToCheck> response = restTemplate.exchange("http://localhost:" + port + "/rest/toCheck/" + toCheck.getId(), HttpMethod.PUT, httpEntity, ToCheck.class);
		assertEquals(200, response.getStatusCodeValue());
		ToCheck updatedToCheck = response.getBody();
		assertEquals(true, updatedToCheck.getIsPrivate());
		assertEquals("Bonjour", updatedToCheck.getCommentaries());
		assertEquals(ToCheckState.NOT_CHECKED, updatedToCheck.getToCheckState());
	}
	
	@Test
	public void testDeleteToCheck() {
		User user1 = new User(null, "hugandhug", "passwordHash", new LinkedList<>(), new LinkedList<>());
		user1 = userRepository.save(user1);
		User user2 = new User(null, "hugandhugg", "passwordHash", new LinkedList<>(), new LinkedList<>());
		user2 = userRepository.save(user2);
		String token1 = JWTUtils.getInstance().generateToken(user1.getId());
		String token2 = JWTUtils.getInstance().generateToken(user2.getId());
		
		Media media = new Media(null, "SpindelHorse Toons", "Helluva Boss", MediaType.SERIES, "");
		media = mediaRepository.save(media);
		ToCheck toCheck1 = new ToCheck(null, media, "", true, LocalDateTime.now(), ToCheckState.CHECKED, user1);
		toCheck1 = toCheckRepository.save(toCheck1);
		ToCheck toCheck2 = new ToCheck(null, media, "", false, LocalDateTime.now(), ToCheckState.CHECKED, user2);
		toCheck2 = toCheckRepository.save(toCheck2);

		// Test1 : Delete and keep media
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", "Bearer " + token1);
		
		ResponseEntity<String> response = restTemplate.exchange("http://localhost:" + port + "/rest/toCheck/" + toCheck1.getId(), HttpMethod.DELETE, new HttpEntity<>(headers), String.class);
		assertEquals(204, response.getStatusCodeValue());
		assertTrue(mediaRepository.findById(toCheck1.getMedia().getId()).isPresent());
		assertFalse(toCheckRepository.findById(toCheck1.getId()).isPresent());
		
		// Test2 : Delete with media
		headers.set("Authorization", "Bearer " + token2);
		
		response = restTemplate.exchange("http://localhost:" + port + "/rest/toCheck/" + toCheck2.getId(), HttpMethod.DELETE, new HttpEntity<>(headers), String.class);
		assertEquals(204, response.getStatusCodeValue());
		assertFalse(mediaRepository.findById(toCheck2.getMedia().getId()).isPresent());
		assertFalse(toCheckRepository.findById(toCheck2.getId()).isPresent());
	}
	
}

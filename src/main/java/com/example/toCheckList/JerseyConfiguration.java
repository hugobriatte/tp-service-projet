package com.example.toCheckList;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import com.example.toCheckList.media.resource.MediaResource;
import com.example.toCheckList.test.resource.TestResource;
import com.example.toCheckList.toCheck.resource.ToCheckResource;
import com.example.toCheckList.user.resource.UserResource;

@Component
@ApplicationPath("rest")
@Configuration
public class JerseyConfiguration extends ResourceConfig {
	public JerseyConfiguration() {
		register(ToCheckResource.class);
		register(UserResource.class);
		register(TestResource.class);
		register(MediaResource.class);
		property(ServletProperties.FILTER_FORWARD_ON_404, true);
	}
}

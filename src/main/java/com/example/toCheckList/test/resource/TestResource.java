package com.example.toCheckList.test.resource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * A simple test resource with an Hello World accesible for everyone
 * @author hugo
 *
 */
@Path("test")
public class TestResource {
	
	@GET
	@Path("hello")
	@Produces(MediaType.TEXT_PLAIN)
	public String getHelloWorld() {
		return "Hello World";
	}

}

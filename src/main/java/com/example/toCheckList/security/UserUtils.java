package com.example.toCheckList.security;

import org.springframework.security.core.context.SecurityContextHolder;

import com.example.toCheckList.user.bdd.User;

/**
 * A useful class to get informations on the currently authentificated user
 * @author Hugo
 *
 */
public class UserUtils {
	
	/**
	 * The one and only instance of UserUtils
	 */
	private static final UserUtils INSTANCE = new UserUtils();
	
	private UserUtils() {
		
	}
	
	/**
	 * Returns the instance of UserUtils
	 * @return The instance of UserUtils
	 */
	public static final UserUtils getInstance() {
		return INSTANCE;
	}
	
	/**
	 * Indicates if the user is authenticated or not
	 * @return true if the user is authenticated 
	 */
	public boolean isAuthenticated() {
		return SecurityContextHolder.getContext().getAuthentication().isAuthenticated();
	}
	
	/**
	 * Returns the username of the currently logged-in user
	 * @return The username of the currently logged-in user, or null if the user is not authenticated
	 */
	public String getUsername() {
		User user = getUser();
		if(user != null) {
			return user.getUsername();
		} else {
			return null;
		}
	}
	
	/**
	 * Returns the currently logged-in user
	 * @return The currently logged-in user, or null if the user is not authenticated
	 */
	public User getUser() {
		if(isAuthenticated()) {
			return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		} else {
			return null;
		}
	}
	
	/**
	 * Returns the ID of the currently logged-in user
	 * @return The ID of the currently logged-in user
	 */
	public Long getUserId() {
		User user = getUser();
		if(user != null) {
			return user.getId();
		} else {
			return null;
		}
	}

}

package com.example.toCheckList.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;

/**
 * A useful class to access useful functions related to JWT
 * @author hugo
 *
 */
public class JWTUtils {
	
	/**
	 * The only instance of JWTUtils
	 */
	private static final JWTUtils INSTANCE = new JWTUtils();
	
	/**
	 * The algorithm used to encrypt and decrypt JWT tokens
	 */
	private Algorithm algorithm = Algorithm.HMAC512("SuperSecretTmp");
	
	private JWTUtils() {
		super();
	}
	
	/**
	 * Returns the main instance of JWTUtils
	 * @return The main instance of JWTUtils
	 */
	public static JWTUtils getInstance() {
		return INSTANCE;
	}
	
	/**
	 * Generate a token based on the given username
	 * @param userName The username to add in the token
	 * @return The generated token or null if there was an error
	 */
	public String generateToken(Long userId) {
		try {
			return JWT.create().withClaim("id", userId).sign(algorithm);
		} catch(JWTCreationException exception) {
			return null;
		}
		
	}
	
	/**
	 * Returns the username contained in a token
	 * @param token The token
	 * @return The username, or null if there was an error
	 */
	public Long getUserId(String token) {
		try {
			DecodedJWT jwt = JWT.require(algorithm).build().verify(token);
			return jwt.getClaim("id").asLong();
		} catch(JWTVerificationException exception) {
			return null;
		}
	}
	
}

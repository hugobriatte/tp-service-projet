package com.example.toCheckList.security;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import com.example.toCheckList.user.resource.UserRepository;

/**
 * A class defining the security configuration of the application
 * @author hugo
 *
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private UserRepository userRepository;
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(username -> userRepository.findOneByUsername(username).orElseThrow());
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// CORS enable and CRSF disabled
		http = http.cors().and().csrf().disable();
		
		// Stateless management of sessions
		http = http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and();
		
		// Send an unauthorized if there isn't a Authorization header
		http = http
	            .exceptionHandling()
	            .authenticationEntryPoint(
	                (request, response, ex) -> {
	                    response.sendError(
	                        HttpServletResponse.SC_UNAUTHORIZED,
	                        ex.getMessage()
	                    );
	                }
	            )
	            .and();
		
		// The full API is made public
		http.authorizeHttpRequests().antMatchers("/**").permitAll();
		
		// Adds the JWT Filter to authentificate users
		http.addFilterBefore(
				new JWTFilter(userRepository),
				UsernamePasswordAuthenticationFilter.class
				);
		
	}

	/**
	 * Configures the CORSFilter used by this server
	 * @return The CORSFilter used by this server
	 */
    @Bean
    public CorsFilter corsFilter() {
    	UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.addAllowedOrigin("localhost:8080");
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");
        source.registerCorsConfiguration("/**", config);
        return new CorsFilter(source);
    }
    
}

package com.example.toCheckList.security;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.HttpHeaders;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import com.example.toCheckList.user.bdd.User;
import com.example.toCheckList.user.resource.UserRepository;

/**
 * A filter used to automatically set the user authentificated based on the token given in the Authorization header
 * @author hugo
 *
 */
public class JWTFilter extends OncePerRequestFilter {
	
	/**
	 * The UserRepository, given at creation of the filter
	 */
	private UserRepository userRepository;
	
	/**
	 * The instance of JWTUtils, to decode JWT tokens
	 */
	private final JWTUtils JWT_UTILS = JWTUtils.getInstance();
	
	public JWTFilter(UserRepository userRepository) {
		super();
		this.userRepository = userRepository;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		// If there is a Bearer authorization in the header
		String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
		if(authHeader != null && authHeader.startsWith("Bearer ")) {
			
			// Find the associated user in the database
			String token = authHeader.substring(7);
			Long id = JWT_UTILS.getUserId(token);
			if(id == null) {
				SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(null, null));
			} else {
				Optional<User> user = userRepository.findById(id);
			
				// If there is someone of the same name in the database, set is as the current user
				if(user.isPresent()) {
					SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(user.get(), token ,user.get().getAuthorities()));
				} else {
					SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(null, null));
				}
			}
		} else {
			SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(null, null));
		}
		
		filterChain.doFilter(request, response);
	}

}

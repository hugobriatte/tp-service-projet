package com.example.toCheckList.toCheck.resource;

import java.time.LocalDateTime;
import java.util.List;
import java.util.NoSuchElementException;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.example.toCheckList.media.resource.MediaRepository;
import com.example.toCheckList.security.UserUtils;
import com.example.toCheckList.toCheck.bdd.ToCheck;
import com.example.toCheckList.user.bdd.User;

/**
 * The ToCheck ressource
 * @author hugo
 *
 */
@Path("toCheck")
public class ToCheckResource {
	
	@Autowired
	private ToCheckRepository toCheckRepository;
	
	@Autowired
	private MediaRepository mediaRepository;
	
	/**
	 * Does the media associated to a ToCheck is editable, or is it a shared one?
	 * @param id The ID of the toCheck
	 * @return true if the media associated is editable, false otherwise
	 */
	@GET
	@Path("/{id}/isMediaEditable")
	@Produces(MediaType.APPLICATION_JSON)
	public boolean isMediaEditable(@PathParam("id") Long id) {
		Long userID = UserUtils.getInstance().getUserId();
		if(userID == null) {
			throw new WebApplicationException(401);
		}
		List<ToCheck> result = toCheckRepository.getEditable(id);
		if(result.isEmpty()) {
			throw new WebApplicationException(404);
		} else {
			return result.get(0).getId().equals(id) && result.get(0).getAssociatedUser().getId().equals(userID);
		}
	}
	
	/**
	 * Create a new ToCheck in the database
	 * @param tc The ToCheck to create
	 * @return The created ToCheck
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ToCheck createToCheck(ToCheck tc) {
		// If an ID has not been mentioned, create a media with the given informations
		if(tc.getMedia().getId() == null) {
			mediaRepository.save(tc.getMedia());
		} else {
			// Otherwise, use the all ready-existent media
			try {
				tc.setMedia(mediaRepository.findById(tc.getMedia().getId()).get());
			} catch(NoSuchElementException e) {
				throw new WebApplicationException(404);
			}
		}
		User user = UserUtils.getInstance().getUser();
		if(user == null) {
			throw new WebApplicationException(401);
		}
		tc.setCreationDate(LocalDateTime.now());
		tc.setAssociatedUser(user);
		return toCheckRepository.save(tc);
	}
	
	/**
	 * Returns a ToCheck based on his ID
	 * @param id The if of the wanted ToCheck
	 * @return The associated ToCheck
	 */
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public ToCheck getToCheck(@PathParam("id") Long id) {
		String username = UserUtils.getInstance().getUsername();
		if(username == null) {
			throw new WebApplicationException(401);
		}
		try {
			ToCheck toCheck = toCheckRepository.findById(id).get();
			if(toCheck.getAssociatedUser().getUsername().equals(username)) {
				return toCheck;
			} else {
				throw new WebApplicationException(403);
			}
		} catch(NoSuchElementException e) {
			throw new WebApplicationException(404);
		}
	}
	
	/**
	 * Returns all the ToCheck of the logged-in user
	 * @return All the ToCheck of the logged-in user
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<ToCheck> getAllToCheck() {
		Long userId = UserUtils.getInstance().getUserId();
		if(userId == null) {
			throw new WebApplicationException(401);
		}
		return toCheckRepository.findByUserId(userId);
	}
	
	/**
	 * Update a ToCheck of given is with new informations
	 * @param id The ToCheck ID
	 * @param toCheck A ToCheck containing the wanted modifications
	 * @return The newly-updated ToCheck
	 */
	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ToCheck updateToCheck(@PathParam("id") Long id, ToCheck toCheck) {
		User user = UserUtils.getInstance().getUser();
		if(user == null) {
			throw new WebApplicationException(401);
		}
		try {
			ToCheck oldToCheck = toCheckRepository.findById(id).get();
			if(oldToCheck.getAssociatedUser().getId().equals(user.getId())) {
				// Set of the excepted values
				oldToCheck.setCommentaries(toCheck.getCommentaries());
				oldToCheck.setIsPrivate(toCheck.getIsPrivate());
				oldToCheck.setToCheckState(toCheck.getToCheckState());
				
				if(toCheck.getMedia() != null) {
					if(toCheck.getMedia().getId() != null) {
						oldToCheck.setMedia(mediaRepository.findById(toCheck.getMedia().getId()).get());
					}
					else if(isMediaEditable(id)) {
						oldToCheck.getMedia().setAuthor(toCheck.getMedia().getAuthor());
						oldToCheck.getMedia().setComment(toCheck.getMedia().getComment());
						oldToCheck.getMedia().setName(toCheck.getMedia().getName());
						oldToCheck.getMedia().setType(toCheck.getMedia().getType());
						mediaRepository.save(oldToCheck.getMedia());
					}
				}
				oldToCheck = toCheckRepository.save(oldToCheck);
				mediaRepository.mediaCleanup();
				return oldToCheck;
			} else {
				throw new WebApplicationException(403);
			}
		} catch(NoSuchElementException e) {
			throw new WebApplicationException(404);
		}
	}
	
	/**
	 * Deletes a ToCheck of given id
	 * @param id The if of the deleted ToCheck
	 */
	@DELETE
	@Path("/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void delete(@PathParam("id") Long id) {
		User user = UserUtils.getInstance().getUser();
		if(user == null) {
			throw new WebApplicationException(401);
		}
		try {
			ToCheck toCheck = toCheckRepository.findById(id).get();
			if(toCheck.getAssociatedUser().getId().equals(user.getId())) {
				toCheckRepository.delete(toCheck);
				mediaRepository.mediaCleanup();
			} else {
				throw new WebApplicationException(403);
			}
		} catch (NoSuchElementException e) {
			throw new WebApplicationException(404);
		}
	}

}

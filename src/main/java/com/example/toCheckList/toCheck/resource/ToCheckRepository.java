package com.example.toCheckList.toCheck.resource;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.example.toCheckList.media.bdd.Media;
import com.example.toCheckList.toCheck.bdd.ToCheck;
import com.example.toCheckList.user.bdd.User;

/**
 * The ToCheck Repository
 * @author hugo
 *
 */
public interface ToCheckRepository extends CrudRepository<ToCheck, Long> {
	
	/**
	 * Returns the ToCheck that has the edition rights of a given toCheck
	 * @param toCheckId The id of the toCheck
	 * @return The ToCheck that has the edition rights of a given toCheck
	 */
	@Query("SELECT t FROM ToCheck t WHERE t.media.id IN (SELECT u.media.id FROM ToCheck u WHERE u.id = :id)"
			+ " ORDER BY is_private, creation_date")
	public List<ToCheck> getEditable(@Param("id") Long checkId);
	
	/**
	 * Returns the number of ToCheck that are associated to a specific media
	 * @param mediaId The ID of the tracked media
	 * @return The numer of ToCheck associated to this media
	 */
	@Query("SELECT COUNT(*) FROM ToCheck WHERE media.id = :id")
	public long countByMediaId(@Param("id") Long mediaId);
	
	/**
	 * Returns a list of ToCheck corresponding to a given userID
	 * @param userId The user ID
	 * @return The list of ToCheck associated to this user
	 */
	@Query("SELECT t FROM ToCheck t WHERE t.associatedUser.id = :userId")
	public List<ToCheck> findByUserId(@Param("userId") Long userId);
	
	/**
	 * Returns a list of ToCheck that has a media of given ID and a user of given ID
	 * @param mediaId The Media ID
	 * @param userId The User ID
	 * @return The list of ToCheck containing the given Media ID and User ID
	 */
	@Query("SELECT t FROM ToCheck t WHERE t.media.id = :mediaId AND t.associatedUser.id = :userId")
	public List<ToCheck> findByMediaIdAndUserId(@Param("mediaId") Long mediaId, @Param("userId") Long userId);

	/**
	 * Returns a list of public ToCheck containing a media of ID given
	 * @param mediaId The Media ID
	 * @return A list of public ToCheck contaning this media
	 */
	@Query("SELECT t FROM ToCheck t WHERE t.media.id = :mediaId AND t.isPrivate = FALSE")
	public List<ToCheck> findPublicOfMediaId(@Param("mediaId") Long mediaId);
	
	/**
	 * Returns all the public Medias
	 * @return All the public Medias
	 */
	@Query("SELECT DISTINCT t.media FROM ToCheck t WHERE t.isPrivate = FALSE")
	public List<Media> getAllPublic();
}

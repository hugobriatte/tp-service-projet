package com.example.toCheckList.toCheck.bdd;

import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.example.toCheckList.media.bdd.Media;
import com.example.toCheckList.user.bdd.User;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * This class represent a media that shall be checked by someone.
 * A Media might be a resource shared by many.
 * A ToCheck shall be associated to a unique user, and can be private.
 * @author hugo
 *
 */
@Entity
public class ToCheck {
	
	/**
	 * The ID of the ToCheck
	 */
	@Id
	@GeneratedValue
	private Long id;
	
	/**
	 * A Media that should be checked
	 */
	@ManyToOne
	private Media media;
	
	/**
	 * A commentary associated to this check
	 */
	private String commentaries;
	
	/**
	 * Shall this toCheck be private or not?
	 */
	private boolean isPrivate;
	
	/**
	 * The date at which this ToCheck has been created
	 */
	private LocalDateTime creationDate;
	
	/**
	 * The current state of the toCheck
	 */
	private ToCheckState toCheckState;
	
	@ManyToOne
	@JsonIgnore
	private User associatedUser;
	
	public ToCheck(Long id, Media media, String commentaries, boolean isPrivate,
			LocalDateTime creationDate, 
			ToCheckState toCheckState, User associatedUser) {
		super();
		this.id = id;
		this.media = media;
		this.commentaries = commentaries;
		this.isPrivate = isPrivate;
		this.creationDate = creationDate;
		this.toCheckState = toCheckState;
		this.associatedUser = associatedUser;
	}

	public ToCheck() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ToCheckState getToCheckState() {
		return toCheckState;
	}

	public void setToCheckState(ToCheckState toCheckState) {
		this.toCheckState = toCheckState;
	}

	public User getAssociatedUser() {
		return associatedUser;
	}

	public void setAssociatedUser(User associatedUser) {
		this.associatedUser = associatedUser;
	}

	public Media getMedia() {
		return media;
	}

	public void setMedia(Media media) {
		this.media = media;
	}

	public String getCommentaries() {
		return commentaries;
	}

	public void setCommentaries(String commentaries) {
		this.commentaries = commentaries;
	}

	public boolean getIsPrivate() {
		return isPrivate;
	}

	public void setIsPrivate(boolean isPrivate) {
		this.isPrivate = isPrivate;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}
	
	public Long getUserId(Long userId) {
		if(getAssociatedUser() != null) {
			return getAssociatedUser().getId();
		} else {
			return null;
		}
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(associatedUser, commentaries, creationDate, id, isPrivate, media, toCheckState);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ToCheck other = (ToCheck) obj;
		return Objects.equals(associatedUser, other.associatedUser) && Objects.equals(commentaries, other.commentaries)
				&& Objects.equals(creationDate, other.creationDate) && Objects.equals(id, other.id)
				&& isPrivate == other.isPrivate && Objects.equals(media, other.media)
				&& toCheckState == other.toCheckState;
	}
	
}

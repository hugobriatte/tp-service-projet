package com.example.toCheckList.toCheck.bdd;

/**
 * A class representing the state of the media
 * @author hugo
 *
 */
public enum ToCheckState {
	
	/**
	 * The media hasn't been checked yet
	 */
	NOT_CHECKED,
	
	/**
	 * The media is currently being checked
	 */
	BEING_CHECKED,
	
	/**
	 * The media has all-ready been checked
	 */
	CHECKED;
	
	/**
	 * Returns a ToCheckState based on a String representation
	 * @param s The String representative of the ToCheckState
	 * @return The ToCheckState associated
	 */
	public static ToCheckState fromString(final String s) {
	    return ToCheckState.valueOf(s.toUpperCase());
	}
}

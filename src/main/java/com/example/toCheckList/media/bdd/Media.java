package com.example.toCheckList.media.bdd;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * A class representing a media, with all its informations
 * @author hugo
 *
 */
@Entity
public class Media {
	
	@Id
	@GeneratedValue
	@Column(nullable = false)
	private Long id;
	
	/**
	 * The name of the media
	 */
	@Column(nullable = false)
	private String name;
	
	/**
	 * The author of the media
	 */
	@Column(nullable = false)
	private String author;
	
	/**
	 * The type of media
	 */
	@Column(nullable = false)
	private MediaType type;

	/**
	 * Commentaries to add on the media
	 */
	@Column(nullable = false)
	private String comment;
	
	public Media(Long id, String name, String author, MediaType type, String comment) {
		super();
		this.id = id;
		this.name = name;
		this.author = author;
		this.type = type;
		this.comment = comment;
	}

	public Media() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public MediaType getType() {
		return type;
	}

	public void setType(MediaType type) {
		this.type = type;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		return Objects.hash(author, comment, id, name, type);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Media other = (Media) obj;
		return Objects.equals(author, other.author) && Objects.equals(comment, other.comment)
				&& Objects.equals(id, other.id) && Objects.equals(name, other.name) && type == other.type;
	}
	
	

}

package com.example.toCheckList.media.bdd;

/**
 * A enum representing several types of medias
 * @author hugo
 *
 */
public enum MediaType {
	
	/**
	 * A series
	 */
	SERIES, 
	
	/**
	 * A movie
	 */
	MOVIE,
	
	/**
	 * A book
	 */
	BOOK, 
	
	/**
	 * A comic book
	 */
	COMIC,
	
	/**
	 * A manga
	 */
	MANGA, 
	
	/**
	 * A game
	 */
	GAME,
	
	/**
	 * Any other media type
	 */
	OTHER;
	
	public static MediaType fromString(final String s) {
	    return MediaType.valueOf(s.toUpperCase());
	}

}

package com.example.toCheckList.media.resource;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.example.toCheckList.media.bdd.Media;

/**
 * The Media Repository
 * @author hugo
 *
 */
public interface MediaRepository extends CrudRepository<Media, Long> {
	
	/**
	 * Removes all the media that are not associated to, at least, one ToCheck
	 */
	@Query("DELETE FROM Media m WHERE m.id NOT IN (SELECT t.media.id FROM ToCheck t)")
	@Transactional
	@Modifying
	public void mediaCleanup();
	
}

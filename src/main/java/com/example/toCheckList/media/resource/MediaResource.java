package com.example.toCheckList.media.resource;

import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.toCheckList.media.bdd.Media;
import com.example.toCheckList.security.UserUtils;
import com.example.toCheckList.toCheck.resource.ToCheckRepository;
import com.example.toCheckList.user.bdd.PublicViewer;
import com.example.toCheckList.user.bdd.User;

/**
 * The Media resource
 * @author hugo
 *
 */
@Path("media")
public class MediaResource {
	
	/**
	 * A local, autowired, MediaRepository
	 */
	@Autowired
	private MediaRepository mediaRepository;
	
	/**
	 * A local, autowired, ToCheckRepositoru
	 */
	@Autowired
	private ToCheckRepository toCheckRepository;
	
	/**
	 * Returns an Media based on his id
	 * @param id The ID of the Media
	 * @return The Media if it's accessible for the user
	 */
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Media getMedia(@PathParam("id") Long id) {
		User user = UserUtils.getInstance().getUser();
		if(user == null) {
			throw new WebApplicationException(401);
		}
		Media media = mediaRepository.findById(id).orElse(null);
		if(media == null) {
			throw new WebApplicationException(404);
		} else if(toCheckRepository.findByMediaIdAndUserId(id, user.getId()).isEmpty()
				&& toCheckRepository.findPublicOfMediaId(id).isEmpty()) {
			throw new WebApplicationException(403);
		} else {
			return media;
		}
	}
	
	/**
	 * Returns a list of PublicViewer that have a ToCheck associated to a Media of given ID
	 * @param id The ID of the media
	 * @return A list of PublicViewer that are associated to this media ID
	 */
	@GET
	@Path("{id}/users")
	@Produces(MediaType.APPLICATION_JSON)
	public List<PublicViewer> getAllUserOfMedia(@PathParam("id") Long id) {
		if(mediaRepository.findById(id).isEmpty()) {
			throw new WebApplicationException(404);
		} else {
			return toCheckRepository.findPublicOfMediaId(id).stream().map(tC -> new PublicViewer(tC.getAssociatedUser().getId(), tC.getAssociatedUser().getUsername(), tC.getToCheckState())).collect(Collectors.toList());
		}
	}
	
	/**
	 * Returns a list of Media that are publically available for everyone
	 * @return The list of Media that are publically available for everyone
	 */
	@GET
	@Path("public")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Media> getAllPublic() {
		return toCheckRepository.getAllPublic();
	}
	

}

package com.example.toCheckList.collection.bdd;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * A class representing informations on a given collection
 * @author hugo
 *
 */
@Entity
public class CollectionInfo {
	
	/**
	 * The ID of the collection
	 */
	@Id
	@GeneratedValue
	private Long id;
	
	/**
	 * The name of the collection
	 */
	private String name;

	public CollectionInfo(Long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public CollectionInfo() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CollectionInfo other = (CollectionInfo) obj;
		return Objects.equals(id, other.id) && Objects.equals(name, other.name);
	}
	
	
	
}

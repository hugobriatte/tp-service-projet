package com.example.toCheckList;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ToCheckListApplication {

	public static void main(String[] args) {
		SpringApplication.run(ToCheckListApplication.class, args);
	}

}

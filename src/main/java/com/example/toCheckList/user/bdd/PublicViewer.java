package com.example.toCheckList.user.bdd;

import java.util.Objects;

import com.example.toCheckList.toCheck.bdd.ToCheckState;

/**
 * A class representing a public viewer of a Media.
 * This class is not present in any databse.
 * PublicViewers are created at runtime with the data of the users
 * @author hugo
 *
 */
public class PublicViewer {
	
	/**
	 * The ID of the user
	 */
	private Long id;
	
	/**
	 * The username of the user
	 */
	private String username;
	
	/**
	 * The state of the ToCheck associated to the PublicViewer
	 */
	private ToCheckState toCheckState;

	public PublicViewer() {
		super();
	}

	public PublicViewer(Long id, String username, ToCheckState state) {
		super();
		this.id = id;
		this.username = username;
		this.toCheckState = state;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public ToCheckState getToCheckState() {
		return toCheckState;
	}

	public void setToCheckState(ToCheckState toCheckState) {
		this.toCheckState = toCheckState;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, toCheckState, username);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PublicViewer other = (PublicViewer) obj;
		return Objects.equals(id, other.id) && toCheckState == other.toCheckState
				&& Objects.equals(username, other.username);
	}
}
package com.example.toCheckList.user.bdd;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.example.toCheckList.collection.bdd.CollectionInfo;
import com.example.toCheckList.toCheck.bdd.ToCheck;

/**
 * A class representing a User of the application
 * @author hugo
 *
 */
@Entity
public class User implements UserDetails {
	
	private static final long serialVersionUID = 1L;

	/**
	 * The ID of the user
	 */
	@Id
	@GeneratedValue
	private Long id;
	
	/**
	 * The username of the user
	 */
	@Column(nullable = false, unique = true)
	private String username;
	
	/**
	 * The passwordHash of the user
	 */
	@Column(nullable = false)
	private String passwordHash;
	
	/**
	 * The list of collections created by this user
	 */
	@OneToMany
	@LazyCollection(LazyCollectionOption.FALSE)
	private Collection<CollectionInfo> collections;
	
	/**
	 * The list of toCheck that belongs to this user
	 */
	@OneToMany(mappedBy = "associatedUser", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
	private Collection<ToCheck> toCheckCollection;

	public User(Long id, String username, String passwordHash, Collection<CollectionInfo> collections,
			Collection<ToCheck> toCheckCollection) {
		super();
		this.id = id;
		this.username = username;
		this.passwordHash = passwordHash;
		this.collections = collections;
		this.toCheckCollection = toCheckCollection;
	}

	public User() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPasswordHash() {
		return passwordHash;
	}

	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}

	public Collection<CollectionInfo> getCollections() {
		return collections;
	}

	public void setCollections(Collection<CollectionInfo> collections) {
		this.collections = collections;
	}

	public Collection<ToCheck> getToCheckCollection() {
		return toCheckCollection;
	}

	public void setToCheckCollection(Collection<ToCheck> toCheckCollection) {
		this.toCheckCollection = toCheckCollection;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return new LinkedList<>();
	}

	@Override
	public String getPassword() {
		return passwordHash;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hash(collections, id, passwordHash, toCheckCollection, username);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		return Objects.equals(collections, other.collections) && Objects.equals(id, other.id)
				&& Objects.equals(passwordHash, other.passwordHash)
				&& Objects.equals(toCheckCollection, other.toCheckCollection)
				&& Objects.equals(username, other.username);
	}

}

package com.example.toCheckList.user.resource;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.example.toCheckList.user.bdd.User;

/**
 * The User Repository
 * @author hugo
 *
 */
public interface UserRepository extends CrudRepository<User, Long> {
	
	/**
	 * Find a User based on his username
	 * @param username The username of the user
	 * @return The User, encapsulated in a Optional
	 */
	Optional<User> findOneByUsername(String username);
	
	/**
	 * Find a User based of his username and password hash
	 * @param username The username of the user
	 * @param passwordHash The password hash
	 * @return The User, encapsulated in a Optional
	 */
	Optional<User> findOneByUsernameAndPasswordHash(String username, String passwordHash);
	
}

package com.example.toCheckList.user.resource;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.example.toCheckList.media.resource.MediaRepository;
import com.example.toCheckList.security.JWTUtils;
import com.example.toCheckList.security.UserUtils;
import com.example.toCheckList.user.bdd.User;

/**
 * The User resource
 * @author hugo
 *
 */
@Path("user")
public class UserResource {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private MediaRepository mediaRepository;
	
	private final JWTUtils JWT_UTILS = JWTUtils.getInstance();
	
	/**
	 * Creates a User with the information given
	 * @param userToCreate The User to create
	 * @return A token associated to the created user
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String createUser(User userToCreate) {
		if(userRepository.findOneByUsername(userToCreate.getUsername()).isPresent()) {
			throw new WebApplicationException(406);
		} else {
			userToCreate = userRepository.save(userToCreate);
			return JWT_UTILS.generateToken(userToCreate.getId());
		}
	}
	
	/**
	 * Authentificate a user based on his username and password
	 * @param userlogin The User object containing the needed information
	 * @return The JWT Token associated to this user
	 */
	@POST
	@Path("login")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String login(User userlogin) {
		userlogin = userRepository.findOneByUsernameAndPasswordHash(userlogin.getUsername(), userlogin.getPasswordHash()).orElse(null);
		if(userlogin != null) {
			return JWT_UTILS.generateToken(userlogin.getId());
		} else {
			throw new WebApplicationException(403);
		}
	}
	
	/**
	 * Deletes the currently logged-in user
	 */
	@DELETE
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void deleteUser() {
		User user = UserUtils.getInstance().getUser();
		if(user == null) {
			throw new WebApplicationException(401);
		} else {
			userRepository.delete(user);
			mediaRepository.mediaCleanup();
		}
	}
	
	/**
	 * Update the username, or the password (or both), of the currently logged-in user
	 * @param userData A User containing the new data
	 */
	@PATCH
	@Consumes(MediaType.APPLICATION_JSON)
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void updatePasswordHash(User userData) {
		User user = UserUtils.getInstance().getUser();
		if(user == null) {
			throw new WebApplicationException(401);
		} else {
			if(userData.getUsername() != null) {
				if(userRepository.findOneByUsername(userData.getUsername()).isPresent()) {
					throw new WebApplicationException(406);
				}
				user.setUsername(userData.getUsername());
			}
			if(userData.getPasswordHash() != null) {
				user.setPasswordHash(userData.getPasswordHash());
			}
			userRepository.save(user);
		}
	}
	
	/**
	 * Returns the username of the currently logged-in user
	 * @return The username of the currently logged-in user
	 */
	@GET
	@Path("username")
	@Produces(MediaType.TEXT_PLAIN)
	public String getUsername() {
		User user = UserUtils.getInstance().getUser();
		if(user == null) {
			throw new WebApplicationException(401);
		} else {
			return user.getUsername();
		}
		
	}
	
}

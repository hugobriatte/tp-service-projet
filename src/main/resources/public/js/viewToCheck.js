import { setAccountHeader } from "./utils/headers.js";
import { getAuthorizationValue } from "./utils/token.js";
import ToCheck from "./utils/toCheck.js";
import PublicViewers from "./utils/publicViewer.js";
import { defaultErrorHandler } from "./utils/errorManager.js";

/**
 * Updates the array of public viewers with a list
 * @param {Array<PublicViewers>} viewerList 
 */
function showPublicViewers(viewerList) {
    const viewerTBody = $("#viewerTBody");
    viewerList.forEach(viewer => {
        viewerTBody.append(`<tr>
            <th scope="row">${viewer.username}</th>
            <td>${viewer.stateFormatted()}</td>
        </tr>`)
    })
}

/**
 * Update the page with the data form the ToCheck
 * @param {ToCheck} toCheck The ToCheck
 */
function showToCheck(toCheck) {
    $.ajax(`/rest/media/${toCheck.media.id}/users`, {
        dataType: "json",
        type: "GET",
        headers: {
            "Authorization": getAuthorizationValue()
        },
        success: function(data) {
            const publicViewers = data.map(e => PublicViewers.fromDict(e));
            showPublicViewers(publicViewers);
        },
        error: defaultErrorHandler
    });

    $("#nameField").text(toCheck.media.name);
    $("#authorField").text(toCheck.media.author);
    $("#typeField").text(toCheck.media.typeFormatted());
    $("#mediaCommentairesField").text(toCheck.media.comment);
    $("#creationDateField").text(toCheck.creationDate.toLocaleDateString());
    $("#stateField").text(toCheck.stateFormatted());
    $("#isPrivateField").text(toCheck.isPrivateFormatted());
    $("#toCheckCommentariesField").text(toCheck.commentaries);
    $("#update").attr("href", `/editToCheck.html?id=${toCheck.id}`);
}

$(document).ready(function() {
    setAccountHeader();
    const urlParams = new URLSearchParams(window.location.search);
    const toCheckId = urlParams.get("id");
    $.ajax(`/rest/toCheck/${toCheckId}`, {
        dataType: "json",
        type: "GET",
        headers: {
            "Authorization" : getAuthorizationValue()
        },
        success: function(data) {
            const toCheck = ToCheck.fromDict(data);
            showToCheck(toCheck);
            $("#delete").click(function() {
                if(window.confirm("Êtes-vous sur de vouloir supprimer ce ToCheck?")) {
                    toCheck.deleteFromDatabase(function() {
                        window.location.href = "/";
                    });
                }
            });
        },
        error: defaultErrorHandler
    })
})
import { setAccountHeader } from "./utils/headers.js";
import { getAuthorizationValue } from "./utils/token.js";
import ToCheck from "./utils/toCheck.js";
import { defaultErrorHandler } from "./utils/errorManager.js";

/**
 * Generate a table row for a given ToCheck
 * @param {ToCheck} toCheck The ToCheck
 * @returns A string representing the table row
 */
function generateTableRow(toCheck) {
    return `
        <tr>
            <th scope="row">${toCheck.id}</th>
            <td>${toCheck.media.name}</td>
            <td>${toCheck.media.typeFormatted()}</td>
            <td>${toCheck.stateFormatted()}</td>
            <td><a class="btn btn-success" href="/viewToCheck.html?id=${toCheck.id}"><i class="bi bi-eye" style="color: white;"></i></a></td>
            <td><button class="btn btn-danger" id="delete-${toCheck.id}"><i class="bi bi-trash"></i></button></td>
        </tr>
    `;
}

/**
 * Update the array with a list of ToCheck
 * @param {Array<ToCheck>} allToCheck 
 */
function updateTab(allToCheck) {
    const tBody = $("tbody");
    tBody.html("");
    allToCheck.map(generateTableRow)
              .forEach(html => {
                  tBody.append(html);
              });
}

/**
 * Show the main array of the ToCheck
 */
function showMainTab() {
    const authorizationValue = getAuthorizationValue();
    if(authorizationValue != null) {
        $("#mainContent").html(`
        <button type="button" class="btn btn-primary">
            <a href="/addToCheck.html"><i class="bi bi-plus-circle" style="color: white;"></i></a>        
        </button>
        <table class="table">
            <thead>
                <tr>
                <th scope="col">ID</th>
                <th scope="col">Nom</th>
                <th scope="col">Type</th>
                <th scope="col">Etat</th>
                <th scope="col"></th>
                <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        `);
        $.ajax("/rest/toCheck", {
                    dataType: "json",
                    type: "GET",
                    headers: {
                        "Authorization" : authorizationValue
                    },
                    success: function(data, status) {
                        let allToCheck = data.map(entry => ToCheck.fromDict(entry));
                        updateTab(allToCheck);
                        allToCheck.forEach(toCheck => {
                            $(`#delete-${toCheck.id}`).click(function() {
                                if(window.confirm(`Voulez-vous supprimer : ${toCheck.shortSummary()}`))
                                toCheck.deleteFromDatabase(function() {
                                    allToCheck = allToCheck.filter(tc => tc.id != toCheck.id);
                                    updateTab(allToCheck);
                                });
                            });
                        });
                    },
                    error: defaultErrorHandler
                }
            );
    }
}

$(document).ready(function() {
    setAccountHeader();
    showMainTab();
});
import { setAccountHeader } from "./utils/headers.js";
import Media from "./utils/media.js";
import ToCheck from "./utils/toCheck.js";
import { getAuthorizationValue } from "./utils/token.js";
import { defaultErrorHandler } from "./utils/errorManager.js";

/**
 * Validate the form and return the ToCheck to add
 * @returns The ToCheck to add if everything is good, null otherwise
 */
function validateToCheck() {
    // First, check the mandatory inputs (name and author)
    let isValid = true;

    const mediaName = $("#nameInput");
    if(mediaName.val() === "" || mediaName.val() === null) {
        mediaName.addClass("is-invalid");
        $("#nameInputErrorHint").text("Champ vide!");
        isValid = false;
    } else {
        mediaName.removeClass("is-invalid");
    }
    
    const mediaAuthor = $("#authorInput");
    if(mediaAuthor.val() === "" || mediaAuthor.val() === null) {
        mediaAuthor.addClass("is-invalid");
        $("#authorInputErrorHint").text("Champ vide!");
        isValid = false;
    } else {
        mediaAuthor.removeClass("is-invalid");
    }

    // If they are good
    if(isValid) {
        // Set the media ID if we use a allready-existing one
        let id;
        const mediaSelectInput = $("#mediaSelectInput");
        if(mediaSelectInput.val() === "NEW") {
            id = null;
        } else {
            id = mediaSelectInput.val()
        }

        // Create and return ToCheck (with the embded Media)
        const creationDate = new Date().toISOString();
        const mediaType = $("#typeInput");
        const mediaCommentaries = $("#mediaCommentariesInput");
        const toCheckState = $("#stateInput");
        const toCheckCommentaries = $("#toCheckCommentariesInput");
        const isPrivate = $("#isPrivateInput");
        const media = new Media(id, mediaName.val(), mediaAuthor.val(), 
                                mediaType.val(), mediaCommentaries.val());
        return new ToCheck(null, media, toCheckCommentaries.val(), 
                           isPrivate.is(":checked"), creationDate, toCheckState.val());
    } else {
        return null;
    }
}

/**
 * Add the ToCheck to the Database
 */
function addToCheck() {
    const toCheck = validateToCheck();
    if(toCheck != null) {
        const loadingSpinner = $("#loadingSpinner");
        loadingSpinner.removeClass("visually-hidden");
        toCheck.addToDatabase(
            function() {
                window.location.href = "/";
            },
            defaultErrorHandler
        ).always(function() {
            loadingSpinner.addClass("visually-hidden");
        });
    }
}

/**
 * Set the media select list in the form
 * @param {Array<Media>} medias The list of medias available
 */
function setMediaList(medias) {
    // Add all the medias to the select input
    const mediaSelect= $("#mediaSelectInput");
    medias.forEach(media => {
        mediaSelect.append(`<option value=${media.id}>${media.shortSummary()}</option>`);
    });
    
    const mediaName = $("#nameInput");
    const mediaAuthor = $("#authorInput");
    const mediaType = $("#typeInput");
    const mediaCommentaries = $("#mediaCommentariesInput");

    // On a change of selected media
    mediaSelect.change(function() {
        // If the user wants to use a premade media
        if(mediaSelect.val() !== "NEW") {
            // Set the inputs to the media value and as read-only
            const selectedMedia = medias.find(e => e.id == mediaSelect.val());
            mediaName.prop("readonly", true);
            mediaName.val(selectedMedia.name);
            mediaAuthor.prop("readonly", true);
            mediaAuthor.val(selectedMedia.author);
            mediaType.prop("disabled", true);
            mediaType.val(selectedMedia.type);
            mediaCommentaries.prop("readonly", true);
            mediaCommentaries.val(selectedMedia.comment);
        } else {
            // Remove the read-only properties
            mediaName.prop("readonly", false);
            mediaAuthor.prop("readonly", false);
            mediaType.prop("disabled", false);
            mediaCommentaries.prop("readonly", false);
        }
    });
}


$(document).ready(function() {
    setAccountHeader();
    $("#addButton").click(addToCheck);
    $.ajax("/rest/media/public", {
        dataType: "json",
        headers: {
            "Authorization" : getAuthorizationValue()
        },
        type: "GET",
        success: function(data) {
            const allPublic = data.map(e => Media.fromDict(e));
            setMediaList(allPublic);
        },
        error: defaultErrorHandler
    });
})
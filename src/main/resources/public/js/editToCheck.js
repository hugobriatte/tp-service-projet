import { setAccountHeader } from "./utils/headers.js";
import { getAuthorizationValue } from "./utils/token.js";
import ToCheck from "./utils/toCheck.js";
import { defaultErrorHandler } from "./utils/errorManager.js";

/**
 * Set the form with the values from the ToCheck
 * @param {ToCheck} toCheck The ToCheck
 */
function showToCheck(toCheck) {

    const nameInput = $("#nameInput");
    const authorInput = $("#authorInput");
    const typeInput = $("#typeInput");
    const mediaCommentariesInput = $("#mediaCommentariesInput");

    // Check if the associated media is editable for the user
    $.ajax(`/rest/toCheck/${toCheck.id}/isMediaEditable`, {
        dataType: "json",
        type: "GET",
        headers: {
            "Authorization" : getAuthorizationValue()
        },
        success: function(isEditable) {
            // If not, set all the associated inputs to readonly
            if(!isEditable) {
                nameInput.prop("readonly", true);
                authorInput.prop("readonly", true);
                typeInput.prop("disabled", true);
                mediaCommentariesInput.prop("readonly", true);
            }
        }
    })

    // Preset all the fields with values from the ToCheck
    nameInput.val(toCheck.media.name);
    authorInput.val(toCheck.media.author);
    typeInput.val(toCheck.media.type);
    mediaCommentariesInput.val(toCheck.media.comment);
    $("#stateInput").val(toCheck.toCheckState);
    const isPrivateInput = $("#isPrivateInput");
    if(toCheck.isPrivate) {
        isPrivateInput.prop("checked", true);
    } else {
        isPrivateInput.prop("checked", false);
    }
    $("#toCheckCommentariesInput").val(toCheck.commentaries);
}

/**
 * Validate the ToCheck form
 * @param {ToCheck} originalToCheck the original ToCheck 
 * @returns The newly created ToCheck
 */
function validateToCheck(originalToCheck) {
    // Check if the mandatory fields are set
    let isValid = true;

    const mediaName = $("#nameInput");
    if(mediaName.val() === "" || mediaName.val() === null) {
        mediaName.addClass("is-invalid");
        $("#nameInputErrorHint").text("Champ vide!");
        isValid = false;
    } else {
        mediaName.removeClass("is-invalid");
    }
    
    const mediaAuthor = $("#authorInput");
    if(mediaAuthor.val() === "" || mediaAuthor.val() === null) {
        mediaAuthor.addClass("is-invalid");
        $("#authorInputErrorHint").text("Champ vide!");
        isValid = false;
    } else {
        mediaAuthor.removeClass("is-invalid");
    }

    // If yes
    if(isValid) {
        // Edit the ToCheck with the new values
        const mediaType = $("#typeInput");
        const mediaCommentaries = $("#mediaCommentariesInput");
        const toCheckState = $("#stateInput");
        const toCheckCommentaries = $("#toCheckCommentariesInput");
        const isPrivate = $("#isPrivateInput");

        originalToCheck.commentaries = toCheckCommentaries.val();
        originalToCheck.isPrivate = isPrivate.is(":checked");
        originalToCheck.toCheckState = toCheckState.val();
        originalToCheck.media.name = mediaName.val();
        originalToCheck.media.author = mediaAuthor.val();
        originalToCheck.media.comment = mediaCommentaries.val();
        originalToCheck.media.type = mediaType.val();
        originalToCheck.media.id = null;

        return originalToCheck;
    } else {
        return null;
    }
}

$(document).ready(function() {
    setAccountHeader();
    const urlParams = new URLSearchParams(window.location.search);
    const toCheckId = urlParams.get("id");
    $.ajax(`/rest/toCheck/${toCheckId}`, {
        dataType: "json",
        type: "GET",
        headers: {
            "Authorization" : getAuthorizationValue()
        },
        success: function(data) {
            const toCheck = ToCheck.fromDict(data);
            showToCheck(toCheck);
            $("#updateButton").click(function() {
                const newToCheck = validateToCheck(toCheck);
                if(newToCheck != null) {
                    const loadingSpinner = $("#loadingSpinner");
                    loadingSpinner.removeClass("visually-hidden");
                    newToCheck.editToDatabase(function() {
                            window.location.href = `/viewToCheck.html?id=${toCheck.id}`;
                        },
                        defaultErrorHandler
                    ).always(function() {
                        loadingSpinner.addClass("visually-hidden");
                    })
                }
            });
        },
        error: defaultErrorHandler
    })
})
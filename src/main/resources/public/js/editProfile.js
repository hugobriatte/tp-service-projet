import { defaultErrorHandler } from "./utils/errorManager.js";
import { setAccountHeader, setUsername } from "./utils/headers.js";
import { getAuthorizationValue } from "./utils/token.js";

const SPINNER_SPAN_HTML = '<span class="spinner-border spinner-border-sm" role="status">';

/**
 * Verify the username field in the form
 * @returns The username associated if everything is good, null otherwise
 */
function verifyUsername() {
    let isValid = true;
    $("#usernameInputErrorHint").text();

    // username verification
    const username = $("#usernameInput");
    if(username.val() === "" || username.val() === null) {
        username.addClass("is-invalid");
        $("#usernameInputErrorHint").text("Champ vide!");
        isValid = false;
    } else {
        username.removeClass("is-invalid");
    }

    if(isValid) {
        return username.val();
    } else {
        return null;
    }
}

/**
 * Update the username
 */
function updateUsername() {
    const newUsername = verifyUsername();
    if(newUsername != null) {
        const usernameButton = $("#usernameSave");
        const oldContent = usernameButton.html();
        usernameButton.html(SPINNER_SPAN_HTML);
        $.ajax(`/rest/user`, {
            data: JSON.stringify({
                username: newUsername
            }),
            dataType: "text",
            contentType: "application/json; charset=utf-8",
            type: "PATCH",
            headers: {
                "Authorization": getAuthorizationValue()
            },
            success: function() {
                setUsername(newUsername);
            },
            error: function(response, textStatus) {
                if(response.status === 406) {
                    $("#usernameInput").addClass("is-invalid");
                    $("#usernameInputErrorHint").text("Nom d'utilisateur déjà pris!");
                }
                defaultErrorHandler(response, textStatus);
            }
        }).always(function() {
            usernameButton.html(oldContent);
        });
    }
}

/**
 * Verify that the password field is correctly set
 * @returns The password if the field is correctly set, null otherwise
 */
function verifyPassword() {
    let isValid = true;
    $("#passwordInputErrorHint").text();

    // password verification
    const password = $("#passwordInput");
    if(password.val() === "" || password.val() === null) {
        password.addClass("is-invalid");
        $("#passwordInputErrorHint").text("Champ vide!");
        isValid = false;
    } else {
        password.removeClass("is-invalid");
    }

    // password confirmation verification
    const passwordConfirmation = $("#passwordConfirmationInput");
    if(passwordConfirmation.val() === "" || passwordConfirmation.val() === null) {
        passwordConfirmation.addClass("is-invalid");
        $("#passwordConfirmationInputErrorHint").text("Champ vide!");
        isValid = false;
    } else {
        isValid = true;
    }

    if(isValid) {
        // Check first if they are the same
        if(passwordConfirmation.val() === password.val()) {
            return passwordConfirmation.val();
        } else {
            password.addClass("is-invalid");
            passwordConfirmation.addClass("is-invalid");
            $("#passwordConfirmationInputErrorHint").text("Les mots de passe sont différents");
            return null;
        }
    } else {
        return null;
    }
}

/**
 * Update the password of the user
 */
function updatePassword() {
    const newPassword = verifyPassword();
    if(newPassword != null) {
        const passwordButton = $("#passwordSave");
        const oldContent = passwordButton.html();
        passwordButton.html(SPINNER_SPAN_HTML);
        $.ajax(`/rest/user`, {
            data: JSON.stringify({
                passwordHash: newPassword
            }),
            dataType: "text",
            contentType: "application/json; charset=utf-8",
            type: "PATCH",
            headers: {
                "Authorization": getAuthorizationValue()
            },
            error: defaultErrorHandler
        }).always(function() {
            passwordButton.html(oldContent);
        });
    }
}

$(document).ready(function () {
    setAccountHeader();
    $.ajax("/rest/user/username", {
                dataType: "text",
                type: "GET",
                headers: {
                    "Authorization" : getAuthorizationValue()
                },
                success: function(data) {
                    $("#usernameInput").val(data);
                },
                error: defaultErrorHandler
            }

        )
    $("#usernameSave").click(updateUsername);
    $("#passwordSave").click(updatePassword);
});
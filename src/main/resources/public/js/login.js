import { setAccountHeader } from "./utils/headers.js";
import { setToken } from "./utils/token.js";
import { defaultErrorHandler } from "./utils/errorManager.js";

/**
 * Validate the signin/login form
 * @returns true if all the fields are correctly set, false otherwise
 */
function validateForm() {
    let isValid = true;
    $("#loginStatus").text("");

    // username verification
    const username = $("#usernameInput");    
    if(username.val() === "" || username.val() === null) {
        username.addClass("is-invalid");
        $("#usernameInputErrorHint").text("Champ vide!");
        isValid = false;
    } else {
        username.removeClass("is-invalid");
    }

    // password verification
    const password = $("#passwordInput");
    if(password.val() === "" || password.val() === null) {
        password.addClass("is-invalid");
        $("#passwordInputErrorHint").text("Champ vide!")
        isValid = false;
    } else {
        password.removeClass("is-invalid");
    }

    return isValid;
}

/**
 * Signin a new user based on the informations given in the login page
 */
function signin() {
    if(validateForm()) {
        const loadingSpinner = $("#loadingSpinner");
        const username = $("#usernameInput");    
        const password = $("#passwordInput");
        loadingSpinner.removeClass("visually-hidden");
        $.ajax("/rest/user", {
            data: JSON.stringify({
                username: username.val(),
                passwordHash: password.val()
            }),
            dataType: "text",
            contentType: "application/json; charset=utf-8",
            type: "POST",
            success: function(data) {
                setToken(data);
                window.location.href = "/";
            },
            error: function(response, textStatus) {
                if(response.status === 406) {
                    $("#formStatus").text("Nom d'utilisateur déjà pris!");
                }
                defaultErrorHandler(response, textStatus);
            }
        }).always(function() {
            loadingSpinner.addClass("visually-hidden");
        });
    }
}

/**
 * Login a new user based on the informations given in the signin page
 */
function login() {
    if(validateForm()) {
        const loadingSpinner = $("#loadingSpinner");
        const username = $("#usernameInput");    
        const password = $("#passwordInput");
        loadingSpinner.removeClass("visually-hidden");
        $.ajax("/rest/user/login", {
            data: JSON.stringify({
                username: username.val(),
                passwordHash: password.val()
            }),
            dataType: "text",
            contentType: "application/json; charset=utf-8",
            type: "POST",
            success: function(data) {
                setToken(data);
                window.location.href = "/";
            },
            error: function(response, textStatus) {
                if(response.status === 403) {
                    $("#formStatus").text("Nom d'utilisateur ou mot de passe incorrect!");
                }
                defaultErrorHandler(response, textStatus);
            }
        }).always(function(data) {
            loadingSpinner.addClass("visually-hidden");
        });
    }
}

$(document).ready(function () {
    setAccountHeader();
    $("#loginButton").click(login);
    $("#signinButton").click(signin);
});
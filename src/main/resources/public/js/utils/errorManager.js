import {removeToken} from "./token.js";

export {defaultErrorHandler}

function defaultErrorHandler(response, textStatus) {
    // If we recieve a 401, that means surely that the token sent is invalid
    if(response.status === 401) {
        removeToken();
    }
    console.error(`${textStatus} - ${response.status}`);
}
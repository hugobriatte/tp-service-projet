import { getToken, removeToken } from "./token.js";
import { defaultErrorHandler } from "./errorManager.js";
export {setAccountHeader, setUsername};

/**
 * Changes the username shown in the header
 * @param {String} username The new username
 */
function setUsername(username) {
    $("#dropdownMenuLink").text(username);
}

/**
 * Generate the Dropdown button on the header, current logged-in user
 * 
 * @param {string} username The name of the current user
 * @returns A HTML text that generates the dropdown button
 */
function generateDropdown(username) {
    return `
    <div class="dropdown">
        <a class="btn btn-info dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
            ${username}
        </a>

        <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
            <li><a class="dropdown-item" href="/editProfile.html">Options utilisateur</a></li>
            <li><a class="dropdown-item" id="logoutButton" href="#">Se déconnecter</a></li>
        </ul>
    </div>
    `
}

/**
 * Set the login button or the account data depending of if the user is loged in or not.
 */
function setAccountHeader() {
    const token = getToken();
    const accountContainer = $("#accountContainer");
    if(token == null) {
        accountContainer.html('<a href="/login.html" class="btn btn-outline-primary">Login</a>');
    } else {
        $.ajax("/rest/user/username", {
                dataType: "text",
                type: "GET",
                headers: {
                    "Authorization" : `Bearer ${token}`
                },
                success: function(data) {
                    accountContainer.html(generateDropdown(data));
                    $("#logoutButton").click(function() {
                        removeToken();
                        window.location.href = "/login.html";
                    });
                },
                error: defaultErrorHandler
            }
        );
    }
}
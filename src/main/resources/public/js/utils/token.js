export {getToken, setToken, getAuthorizationValue, removeToken}

/**
 * Returns a authentification token to use during HTTP request
 * @returns The authentification token to user
 */
function getToken() {
	return window.localStorage.getItem("token");
}

/**
 * Returns the value of the Authorization header to user during HTTP request
 * @returns The value of the Authorization header
 */
function getAuthorizationValue() {
	const token = getToken();
	if(token == null) {
		return null;
	} else {
		return `Bearer ${getToken()}`;
	}
}

/**
 * Set a new value for the authentification token
 * @param {string} value The new authentification token
 */
function setToken(value) {
	window.localStorage.setItem("token", value);
}

/**
 * Remove the currently saved authentification token
 */
function removeToken() {
	window.localStorage.removeItem("token");
}
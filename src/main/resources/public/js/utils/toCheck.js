import Media from "./media.js";
import { getAuthorizationValue } from "./token.js";

export default class ToCheck {

    /**
     * Create a ToCheck
     * @param {int} id The id
     * @param {Media} media The Media to check
     * @param {String} commentaries Commentaries associated to the ToCheck
     * @param {Boolean} isPrivate Is the ToCheck private?
     * @param {Date} creationDate The creation date
     * @param {String} toCheckState The ToCheckState
     */
    constructor(id, media, commentaries, isPrivate, creationDate, toCheckState) {
        this.id = id;
        this.media = media;
        this.commentaries = commentaries;
        this.isPrivate = isPrivate;
        this.creationDate = creationDate;
        this.toCheckState = toCheckState;
    }

    /**
     * Creates a ToCheck from a dictionnary object
     * @param {Object} dict A dictionnary object
     * @returns The ToCheck created
     */
    static fromDict(dict) {
        return new ToCheck(dict.id,
                           Media.fromDict(dict.media),
                           dict.commentaries,
                           dict.isPrivate,
                           new Date(dict.creationDate),
                           dict.toCheckState);
    }

    /**
     * Delete the ToCheck from the database
     * @param {Function} successFunction A function to call if the delete suceeded
     * @param {Function} errorFunction A function to call if the delete failed
     * @returns The associated promise
     */
    deleteFromDatabase(successFunction, errorFunction) {
        return $.ajax(`/rest/toCheck/${this.id}`, {
            dataType: "text",
            type: "DELETE",
            headers: {
                "Authorization": getAuthorizationValue()
            },
            success: successFunction,
            error: errorFunction
        });
    }

    /**
     * Returns the state formatted correctly
     * @returns The state formatted correctly
     */
    stateFormatted() {
        switch(this.toCheckState) {
            case 'NOT_CHECKED':
                return "Pas checké";
            case 'BEING_CHECKED':
                return "En cours de check";
            case 'CHECKED':
                return "Checké";
        }
    }

    /**
     * Return the isPrivate as a String
     * @returns The IsPrivate as a String
     */
    isPrivateFormatted() {
        if(this.isPrivate) {
            return "Oui";
        } else {
            return "Non";
        }
    }

    /**
     * Returns a Short Summary of the media of this ToCheck
     * @returns A String representing a short summary
     */
    shortSummary() {
        return `${this.media.shortSummary()} - ${this.stateFormatted()}`;
    }

    /**
     * Adds the ToCheck to the DataBase
     * @param {Function} successFunction The function to execute if everything went right
     * @param {Function} errorFunction The function to execute if it didn't went well
     * @returns The promise associated
     */
    addToDatabase(successFunction, errorFunction) {
        return $.ajax("/rest/toCheck", {
            data: JSON.stringify(this),
            dataType: "text",
            contentType: "application/json; charset=utf-8",
            headers: {
                "Authorization" : getAuthorizationValue()
            },
            type: "POST",
            success: successFunction,
            error: errorFunction
        })
    }

    /**
     * Edits the ToCheck to the DataBase
     * @param {Function} successFunction The function to execute if everything went right
     * @param {Function} errorFunction The function to execute if it didn't went well
     * @returns The promise associated
     */
    a
    editToDatabase(successFunction, errorFunction) {
        return $.ajax(`/rest/toCheck/${this.id}`, {
            data: JSON.stringify(this),
            dataType: "text",
            contentType: "application/json; charset=utf-8",
            headers: {
                "Authorization": getAuthorizationValue()
            },
            type: "PUT",
            success: successFunction,
            error: errorFunction
        });
    }
}
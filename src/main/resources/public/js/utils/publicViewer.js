export default class PublicViewer {

    /**
     * Creates a PublicViewer from the id, username and toCheckState given
     * @param {int} id The public ID
     * @param {String} username The username to create
     * @param {String} toCheckState The ToCheckState
     */
    constructor(id, username, toCheckState) {
        this.id = id;
        this.username = username;
        this.toCheckState = toCheckState;
    }

    /**
     * Creates a PublicViewer from a dictionnary object
     * @param {Object} dict The dictionnary object
     * @returns The PublicViewer associated
     */
    static fromDict(dict) {
        return new PublicViewer(dict.id, dict.username, dict.toCheckState);
    }

    /**
     * Returns the state formatted correctly
     * @returns The state formatted correctly
     */
    stateFormatted() {
        switch(this.toCheckState) {
            case 'NOT_CHECKED':
                return "Pas checké";
            case 'BEING_CHECKED':
                return "En cours de check";
            case 'CHECKED':
                return "Checké";
        }
    }
}
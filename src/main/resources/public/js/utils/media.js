export default class Media {
    /**
     * Creates a new Media object
     * @param {int} id The ID of the media object
     * @param {String} name The name of the media object
     * @param {String} author The author of the media object
     * @param {String} type The type of the media object
     * @param {String} comment Commentaries associated to the media object
     */
    constructor(id, name, author, type, comment) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.type = type;
        this.comment = comment
    }

    /**
     * Creates a Media from a dictionnary
     * @param {Object} dict A dictionnary of object
     * @returns The Media object
     */
    static fromDict(dict) {
        return new Media(dict.id, dict.name, dict.author,
                         dict.type, dict.comment);
    }

    /**
     * Returns the type of the Media in a more readable way
     * @returns The type of the Media in a more readable way
     */
    typeFormatted() {
        switch(this.type) {
            case "SERIES":
                return "Série";
            case "MOVIE":
                return "Film";
            case "BOOK":
                return "Livre";
            case "COMIC":
                return "BD/Comic";
            case "MANGA":
                return "Manga";
            case "GAME":
                return "Jeu";
            default:
                return "Autre";
        }
    }

    /**
     * Shortly resumes the Media Object
     * @returns A String representing a short description of the Object
     */
    shortSummary() {
        return `${this.name} - ${this.author} (${this.typeFormatted()})`;
    }

}
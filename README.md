# ToCheckList

ToCheckList est un site internet dévelopé dans le cadre du projet du module "Architecture à service" par Hugo BRIATTE.
Ce site est une interface vers une base de données permettant aux utilisateurs de conserver une liste de médias culturel à regarder.

## Comment lancer le projet?

### Base de données

Nous vous conseillons d'utiliser une base de données HSQLDB. Veillez donc à avoir une instance de HSQLDB lancée, et à actualiser la propiété "spring.datasource.driver-class-name" dans le fichier "/src/main/resources/".
Si vous vouliez utiliser une autre base de donnée, veillez aussi à modifier la propiété "spring.datasource.driver-class-name" avec le nom de la classe driver JDBC associé à cette BDD. Vous devriez surement actualiser le pom.xml pour inclure ces drivers

### Le serveur

Le serveur REST et le serveur web est conjoint au sein d'un même serveur.
Pour lancer le serveur, avec mvn, lancez la commande :

```
mvn spring-boot:run
```

Le serveur est prévu pour fonctionner sur localhost, et sur le port 8080. Si changement, veillez à changer aussi les propiétés CORS dans la classe "SecurityConfig".
